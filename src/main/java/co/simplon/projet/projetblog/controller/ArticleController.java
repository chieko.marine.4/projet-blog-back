package co.simplon.projet.projetblog.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.projet.projetblog.entity.Article;
import co.simplon.projet.projetblog.repository.ArticleRepository;
import jakarta.validation.Valid;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepo;

    @GetMapping
    public List<Article> all() {
        return articleRepo.findAll();
    }
    
    @GetMapping("/{id}")
    public Article one(@PathVariable int id) {
        Article article = articleRepo.findById(id);
        if(article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setVue(article.getVue()+1);
        articleRepo.update(article);
        return article;
    }

        //-----essaie
        // @GetMapping("/search/{mot}")
        // public Article one(@PathVariable String mot) {
        //     Article article = articleRepo.findByMot(mot);
        //     if(article == null) {
        //         throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        //     }
        //     return article;
        // }

    @GetMapping("/search/{mot}")
    public List<Article> allMot(@PathVariable String mot) {
        return articleRepo.findAllMot(mot);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article add( @Valid @RequestBody Article article) {
        //ffaire set date sur l'article pour lui donner la date de maintenant
        article.setDate_article(LocalDate.now());
        articleRepo.persist(article);
        return article;
    }
    //erreur 
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        articleRepo.delete(id);
    }
    
    @PutMapping("/{id}")
    public Article replace(@PathVariable int id, @Valid @RequestBody Article article) {
        one(id); 
        article.setId(id);
        articleRepo.update(article);
        return article;
    }
    
    @GetMapping("/categorie/{id}")
    public List<Article> idCategorie(@PathVariable int id){
        return articleRepo.findByIdCategorie(id);
    }
    
    //--essaie
    // @GetMapping("/categorie/one{id}")
    // public Article article(@PathVariable int id) {
    //     Article article = articleRepo.findByIdCategorie(id);

    //     if(article == null) {
    //         throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    //     }
    //     return article;
    // }



}
